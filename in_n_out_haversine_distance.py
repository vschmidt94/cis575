"""
CIS575 Team Project - Fall 2017
Script to find distance from a given lat, long cordinate
to the nearest In-N-Out Burger location in California
"""

from haversine import haversine
import csv
import urllib2
import json
import datetime

homes_file = "properties_2016.csv"
out_file = "out.csv"


#---------------------------------------------------------------------------------------
def get_burger_list():
    """
    Uses In-N-Out Burger's Web API to build a list of all locations in 
    California
    """

    url = "http://locations.in-n-out.com/api/finder/state/CA"
    response = urllib2.urlopen(url).read()
    burger_list = json.loads(response)

    return burger_list

#---------------------------------------------------------------------------------------
def find_closest_burger(lat_long, b_list):

    min_dist = 999;
    store_id = -1
    for e in b_list:
        b_ll = ( float(e["Latitude"]), float(e["Longitude"]))
        dist = haversine( lat_long, b_ll )
        if( dist < min_dist ):
            store_id = int(e["StoreNumber"])
            min_dist = dist

    return [min_dist, store_id];


#---------------------------------------------------------------------------------------
def process_homes(f_obj):
    """
    Takes a CSV file of homes and finds the distance to
    the nearest in-n-out burger. Outputs the results as
    a new CVS file.
    """

    # Get the list of in-n-out burger locations in CA
    b_list = get_burger_list()

    # Set up CSV file reader for the home list
    f_reader = csv.DictReader(f_obj)

    with open(out_file, "wb") as csv_out_file:
        time_start = datetime.datetime.now()
        i = 0
        sum = 0
        writer = csv.writer(csv_out_file, delimiter=',')
        for home in f_reader:
            
            if home['propertylandusetypeid'] != '261':
                continue

            lat_long=( float(home['latitude'])/1000000, float(home['longitude'])/1000000)
            closest = find_closest_burger( lat_long, b_list )
            id = home['parcelid']
            row = [id, closest[0], closest[1]]
            writer.writerow(row)
            print(row)
            i += 1
            sum += closest[0]
        
        time_end = datetime.datetime.now()
        delta = time_end - time_start
        print( "Processed %d homes in: ", i)
        print( delta.__str__())
        print( "Average Distance: %f", sum/i )


#---------------------------------------------------------------------------------------
if __name__ == "__main__":
    
    with open(homes_file, "rb") as homes_f:
        process_homes(homes_f)
